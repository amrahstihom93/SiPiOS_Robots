#! /bin/bash

# First we destroy any remnants of the previous SiPiOS Robots.
sudo rm -r /tmp/si_update
sudo rm -r /home/pi/Desktop/SiPiOS_Robots/

mkdir /home/pi/si_update
cd /home/pi/si_update
git clone https://github.com/amrahstihom93/SiPiOS_Robots/
cd SiPiOS_Robots

sudo sh update_master.sh

echo " █████╗ ██╗     ███████╗██████╗ ████████╗";
echo "██╔══██╗██║     ██╔════╝██╔══██╗╚══██╔══╝";
echo "███████║██║     █████╗  ██████╔╝   ██║   ";
echo "██╔══██║██║     ██╔══╝  ██╔══██╗   ██║   ";
echo "██║  ██║███████╗███████╗██║  ██║   ██║   ";
echo "╚═╝  ╚═╝╚══════╝╚══════╝╚═╝  ╚═╝   ╚═╝   ";
echo "                                         ";
echo "VERY IMPORTANT TO NOTE:  ";
echo "This will change your hostname from";
echo "raspberrypi to sipi";  
echo "To logon, use pi@sipi.local";

echo "Press Enter to EXIT"
read