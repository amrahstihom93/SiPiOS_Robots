#! /bin/bash
#
# Set some details to fit SiPiOS Robots
# mainly :
# set the password for user Pi to bots1234
# set samba password to bots1234
# set hostname to sipi
# by Mohit Sharma // Sigmaway
#
##########################################

echo "==================================="
echo "=            Sit Down             ="
echo "==================================="

#########################################
##
## 1. User Name : pi
## 2. Password  : bots1234
## 3. Hostname  : sipi
## 4. Installing Samba
## 5. Installing tightvncserver
##
#########################################
DEFAULT_PWD=bots1234

#########################################
##
## Changing Pi password to bots1234
##
#########################################
echo pi:$DEFAULT_PWD | sudo chpasswd

####################################
# Installing Samba
####################################
echo "********** SAMBA ***********"
sudo apt-get install -y samba samba-common
sudo cp /etc/samba/smb.conf .
sudo chown pi:pi smb.conf
sudo sed -i 's/read only = yes/read only = no/g' smb.conf
sudo echo 'security = user' >> smb.conf

sudo chown root:root smb.conf
sudo cp smb.conf /etc/samba/smb.conf
sudo systemctl restart smbd.service

# smbpasswd won't take a redirection of the type 
# echo $1\r$1| smbpassd -a pi
# so we do it the long way
echo $DEFAULT_PWD > smbpasswd.txt
echo $DEFAULT_PWD >> smbpasswd.txt
sudo smbpasswd -a pi < smbpasswd.txt
sudo rm smbpasswd.txt # sudo not needed, it's there to be consistent and look pretty
sudo rm smb.conf

####################################
# Set hostname to sipi
####################################
# Re-write /etc/hosts
echo "Editing hosts file"
sudo sed 's/raspberrypi/sipi/g' </etc/hosts > hosts
sudo cp hosts /etc/hosts
rm hosts

echo "Creating new hostname."
echo 'sipi' > ./hostname
sudo cp ./hostname /etc/hostname
rm ./hostname
echo "New hostname file created."

echo "Commiting hostname change."
sudo /etc/init.d/hostname.sh

echo "Hostname change will be effective after a reboot"


####################################
# installing tightvncserver
# tightvncserver will only work after a reboot - not done here
####################################
echo "********** TIGHTVNCSERVER ***********"
sudo wget https://gitlab.com/amrahstihom93/SiPiOS_Robots/raw/master/sipi_update/tightvncserver.sh
#sudo chmod +x tightvncserver.sh
sudo bash ./tightvncserver.sh $DEFAULT_PWD
sudo rm tightvncserver.sh


####################################
# install noNVC
# this is not yet according to the GoSigmaway, 
# and still being developed and looked at
# it's being downloaded in the wrong place, for starter
####################################
echo "********** NOVNC ***********"
wget https://gitlab.com/amrahstihom93/SiPiOS_Robots/raw/master/sipi_update/novnc.sh
#chmod +x novnc.sh
sudo bash ./novnc.sh
sudo rm novnc.sh