#! /bin/bash
#
# Add LX Desktop And Raspberry Pi to Jessie
# by Mohit Sharma // Sigmaway
#
###################################################
#
# To Setup and Run:
# cd /home/pi/
# sudo wget https://gitlab.com/amrahstihom93/SiPiOS_Robots/raw/master/sipi_update/sipi_upgrade.sh
# sudo chmod +x sipi_upgrade.sh
# sudo ./sipi_upgrade.sh
#
###################################################

###################################################
# remove all extraneous packages from Jessie 
# trying to gain as much space as possible
# change CleoQc to DexterInd when doing PR
###################################################
rm ./sipi_cleanup.sh  # just making sure there isn't one yet
wget https://gitlab.com/amrahstihom93/SiPiOS_Robots/raw/master/sipi_update/sipi_cleanup.sh
chmod +x ./sipi_cleanup.sh
./jessie_cleanup.sh
rm ./jessie_cleanup.sh

###################################################
##
## Update and Upgrade the system
##
###################################################
sudo apt-get update -y
sudo apt-get upgrade -y

###################################################
##
## apache and avahi setup
##
###################################################
sudo apt-get -y install avahi-daemon avahi-utils
sudo rm /etc/avahi/avahi-daemon.conf 														# Remove Avahi Config file.
sudo cp /home/pi/di_update/SiPiOS_Robots/upd_script/avahi-daemon.conf /etc/avahi 				# Copy new Avahi Config File.
sudo chmod +x /etc/avahi/avahi-daemon.conf 
sudo apt-get install apache2 -y
sudo apt-get install php5 libapache2-mod-php5 -y
sudo apt-get install raspberrypi-net-mods -y
sudo apt-get install wpagui -y

###################################################
##
## Install/Update Scratch
##
###################################################
sudo apt-get install scratch -y

###################################################
##
## Install Python tools
##
###################################################
sudo apt-get install avahi-autoipd bc python-imaging python-pexpect python-renderpm -y
sudo apt-get install python-reportlab python-reportlab-accel -y
sudo apt-get install python-tk python3-tk idle idle-python2.7 idle3 nodejs nodejs-legacy -y
sudo apt-get install pypy-setuptools pypy-upstream pypy-upstream-dev pypy-upstream-doc blt -y
sudo apt-get install bluetooth -y
sudo apt-get install git -y

###################################################
##
## Backup updates
##
###################################################
sudo https://gitlab.com/amrahstihom93/SiPiOS_Robots/raw/master/update_backup.sh /home/pi
sudo chmod +x update_backup.sh
sudo ./update_backup.sh
rm ./update_backup.sh -y


###################################################
##
## Install final tools and setup your sipios
##
###################################################
rm ./sipi_custom.sh
wget https://gitlab.com/amrahstihom93/SiPiOS_Robots/raw/master/sipi_update/sipi_custom.sh
chmod +x ./sipi_custom.sh
./sipi_custom.sh
rm ./sipi_custom.sh

##################################################
##
## cd /home/pi/Desktop
## sudo git clone <repo add>
## cd /home/pi/Desktop
## sudo git clone <repo add>
##
##################################################
cd home/pi/Desktop
sudo sh /home/pi/di_update/SiPiOS_Robots/update_master.sh


sudo apt-get clean 
sudo apt-get autoremove -y