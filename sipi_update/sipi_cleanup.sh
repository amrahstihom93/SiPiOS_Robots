# Remove bloatwares from Raspbian Jessie.

##################################################
##
## Saving the list of available packages at start.
##
##################################################
dpkg --get-selections | grep -v deinstall > atstart_packages.txt

########################################
##
## Removing some stuff for real!!!
##
########################################
sudo apt-get purge -y wolfram-engine
sudo apt-get purge -y libreoffice*
sudo apt-get purge -y greenfoot claws-mail* cryptsetup-bin
sudo apt-get purge -y debian-reference-common debian-reference-engine
sudo apt-get purge -y minecraft-pi
sudo apt-get purge -y supercollider*

######################################################
##
## Update and Upgrade the OS from debian repositories.
##
######################################################
sudo apt-get update
sudo apt-get upgrade -y

sudo apt-get clean
sudo apt-get autoremove -y

######################################################
##
## Saving the list of available packages after update.
##
######################################################
dpkg --get-selections | grep -v deinstall > atend_packages.txt
