#! /bin/bash
# Another Backup Option.  
# Run this with "sudo sh ~/update_backup.sh" in the command line.

sudo rm -r /home/pi/si_update

mkdir /home/pi/si_update
cd /home/pi/si_update
sudo git clone https://gitlab.com/amrahstihom93/SiPiOS_Robots
cd SiPiOS_Robots

cd /home/pi/si_update/SiPiOS_Robots/


sudo chmod +x /home/pi/si_update/SiPiOS_Robots/update_master.sh
sudo chmod +x /home/pi/si_update/SiPiOS_Robots/upd_script/update_all.sh
sudo chmod +x /home/pi/si_update/SiPiOS_Robots/sipios_robots_update.py

sudo apt-get install python-wxgtk2.8 python-wxtools wx2.8-i18n --force-yes -y			# Install wx for python for windows / GUI programs.
echo "Installed wxpython tools"
sudo apt-get install python-psutil --force-yes -y
echo "Python-PSUtil"
